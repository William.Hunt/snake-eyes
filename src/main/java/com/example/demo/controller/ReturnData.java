package com.example.demo.controller;

public class ReturnData {
    private int dice1;
    private int dice2;
    private int stake;
    private int winnings;
    private String payout_name;

    ReturnData() {
    }

    ReturnData(int dice1, int dice2, int stake, int winnings, String payout_name) {
        this.dice1 = dice1;
        this.dice2 = dice2;
        this.stake = stake;
        this.winnings = winnings;
        this.payout_name = payout_name;
    }

    public String getPayout_name() {
        return payout_name;
    }

    void setPayout_name(String payout_name) {
        this.payout_name = payout_name;
    }

    public int getDice1() {
        return dice1;
    }

    void setDice1(int dice1) {
        this.dice1 = dice1;
    }

    public int getDice2() {
        return dice2;
    }

    void setDice2(int dice2) {
        this.dice2 = dice2;
    }

    public int getStake() {
        return stake;
    }

    void setStake(int stake) {
        this.stake = stake;
    }

    public int getWinnings() {
        return winnings;
    }

    void setWinnings(int winnings) {
        this.winnings = winnings;
    }
}
