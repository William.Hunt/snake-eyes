package com.example.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Array;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/snakeeyes")
public class DemoController {

    @ResponseBody
    @RequestMapping(value = "/play/{stake}", method = GET, produces = "application/json")
    public String demo(@PathVariable("stake") int stake) throws JsonProcessingException {
        int[] dice = GetRandomDice();
        int winnings = 0;
        String payoutName = "loss";
        if (dice[0] == dice[1]) {
            if (dice[0] == 1) {
                winnings = stake * 30;
                payoutName = "snake eyes";
            } else {
                winnings = stake * 7;
                payoutName = "win";
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new ReturnData(dice[0], dice[1], stake, winnings, payoutName));
    }

    public int[] GetRandomDice() {
        String value;
        RestTemplate restTemplate = new RestTemplate();
        value = Objects.requireNonNull(restTemplate.getForObject("https://www.random.org/integers/?num=2&min=1&max=6&col=2&base=10&format=plain", String.class)).trim();
        String[] values = value.split("\t");
        return new int[]{Integer.parseInt(values[0]), Integer.parseInt(values[1])};
    }
}

