package com.example.demo;

import com.example.demo.controller.DemoController;
import com.example.demo.controller.ReturnData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Test
    public void testRandomDice() {
        System.out.println("Dice Test Start");
        DemoController demo = new DemoController();
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
        int[] dice = demo.GetRandomDice();
        assertEquals(2, dice.length);
        assertTrue(list.contains(dice[0]));
        assertTrue(list.contains(dice[1]));
        System.out.println("Dice Test Complete");
    }

    @Test
    public void testReturnedArray() throws IOException {
        System.out.println("data Test Start");
        DemoController demo = new DemoController();
        String returnVal;
        returnVal = demo.demo(10);
        ObjectMapper mapper = new ObjectMapper();
        ReturnData data = mapper.readValue(returnVal, ReturnData.class);
        if (data.getDice1() == data.getDice2()) {
            if (data.getDice1() == 1) {
                assertEquals("snake eyes", data.getPayout_name());
                assertEquals(300, data.getWinnings());
            } else {
                assertEquals("win", data.getPayout_name());
                assertEquals(70, data.getWinnings());
            }
        } else {
            assertEquals("loss", data.getPayout_name());
            assertEquals(0, data.getWinnings());
        }

        System.out.println("data Test Complete");

    }

}
